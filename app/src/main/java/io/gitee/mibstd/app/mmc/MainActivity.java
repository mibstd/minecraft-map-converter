package io.gitee.mibstd.app.mmc;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Runnable {
    File file;
    String out;
    TextView output;
    String path;
    private void log(final String str) {
        runOnUiThread(new Runnable() {
            public void run() {
                MainActivity.this.output.append(str);
            }
        });
    }
    private void rename(String str) {
        if (new File(this.path + str).isDirectory()) {
            copyFolder(this.path + str, this.out + str);
            return;
        }
        copyFile(this.path + str, this.out + str);
    }
    private void rename(String str, String str2) {
        String str3 = this.path + str;
        String str4 = this.path + str2;
        if (new File(str3).isDirectory()) {
            copyFolder(str3, str4);
        } else {
            copyFile(str3, str4);
        }
    }
    public void copyFile(String str, String str2) {
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            FileOutputStream fileOutputStream = new FileOutputStream(str2);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileInputStream.close();
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void copyFolder(String str, String str2) {
        log("copying:" + str + "\n");
        try {
            new File(str2).mkdirs();
            String[] list = new File(str).list();
            for (String str3 : list) {
                File file2 = new File(str, str3);
                if (file2.isDirectory()) {
                    copyFolder(str + "/" + str3, str2 + "/" + str3);
                } else {
                    FileInputStream fileInputStream = new FileInputStream(file2);
                    FileOutputStream fileOutputStream = new FileOutputStream(str2 + "/" + file2.getName());
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    fileInputStream.close();
                    fileOutputStream.flush();
                    fileOutputStream.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void onClick(View view) {
        File file2 = new File(((EditText) findViewById(R.id.et)).getText().toString());
        this.file = file2;
        this.path = file2.getAbsolutePath();
        this.out = this.file.getParent() + "/output";
        if (!this.file.exists()) {
            Toast.makeText(this, "The path you entered does not exist", 1).show();
            return;
        }
        new File(this.out).delete();
        new Thread(this).start();
    }
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        this.output = (TextView) findViewById(R.id.output);
        findViewById(R.id.button).setOnClickListener(this);
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 0);
    }
    public void run() {
        String str;
        try {
            rename("/world/advancements");
            rename("/world/data");
            rename("/world/datapacks");
            rename("/world/playerdata");
            rename("/world/poi");
            rename("/world/region");
            rename("/world/stats");
            rename("/world/icon.png");
            rename("/world/level.dat");
            rename("/world/level.dat_old");
            rename("/world/session.lock");
            rename("/world/uid.dat");
            rename("/world/data", "/world_nether/data");
            rename("/world/DIM-1", "/world_nether/DIM-1");
            rename("/world/level.dat", "/world_nether/level.dat");
            rename("/world/level.dat_old", "/world_nether/level.dat_old");
            rename("/world/session.lock", "/world_nether/session.lock");
            rename("/world/uid.dat", "/world_nether/uid.dat");
            rename("/world/data", "/world_the_end/data");
            rename("/world/DIM1", "/world_the_end/DIM1");
            rename("/world/level.dat", "/world_the_end/level.dat");
            rename("/world/level.dat_old", "/world_the_end/level.dat_old");
            rename("/world/session.lock", "/world_the_end/session.lock");
            rename("/world/uid.dat", "/world_the_end/uid.dat");
            str = "complete";
        } catch (Throwable t) {
            str = t.toString();
        }
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MainActivity.this, str, 1).show();
            }
        });
    }
}