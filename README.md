# Minecraft-Map-Converter

### Introduction
Simple map convertion between the minecraft server side world and the java editon's world.

### Install & Use
1. Download the latest release [here](https://gitee.com/mibstd/minecraft-map-converter/releases).
2. Enter the path of your map and click the button.
3. Enjoy.

### Troubleshooting
There isn't a known issue yet. Please feel free to submit an issue.

### TODO
Better GUI

### Contributing
You are welcomed to contribute to this project! You can start a pull request or submit an issue if you would like to contribute.

### Note
Currently, this app only supports converting the minecraft server side world to the java editon's world.
If this app helps you, don't forget to leave a star!